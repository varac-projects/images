# Public container images

- Container registry: https://0xacab.org/varac-projects/images/container_registry

## Push example

Logged in already ?

    $ docker login --get-login registry.0xacab.org
    Error: not logged into registry.0xacab.org

Login:

    docker login -u varac -p $(gopass show --password token/0xacab.org/varac/cli-git) registry.0xacab.org

Build and push:

    docker build -t registry.0xacab.org/varac-projects/images/gluetun:1ac031e78c .
    docker push registry.0xacab.org/varac-projects/images/gluetun:1ac031e78c

Search for tags:

    $ podman search --list-tags registry.0xacab.org/varac-projects/images/gluetun
    NAME                                               TAG
    registry.0xacab.org/varac-projects/images/gluetun  1ac031e78c
