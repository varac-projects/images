# Python Playwright container with Firefox

The [official python-playwright](https://mcr.microsoft.com/en-us/product/playwright/python/tags) images

- are huge, because they contain all browsers (> 2 GB)
- [Don't have python-playwright installed](https://github.com/microsoft/playwright-python/issues/1978) anymore,
  so it needs to get installed after container start
  (`python -m pip install playwright`)

- [Official Dockerfile.noble](https://github.com/microsoft/playwright-python/blob/main/utils/docker/Dockerfile.noble)
  - Based on [ubuntu:noble](https://hub.docker.com/_/ubuntu) which is based on [`buildpack-deps`](https://hub.docker.com/_/buildpack-deps)
    - [Multiple Python versions in the image](https://github.com/docker-library/docs/tree/master/python#multiple-python-versions-in-the-image)
      - "In the non-slim variants there will be an additional
        (distro-provided) python executable..."
